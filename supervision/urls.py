from django.urls import path
from django.contrib import admin

from supervision.views import (
    TimelinessListView, EffectivenessListView,
    ProcurementListView, RepairmentListView)

urlpatterns = [
    path('timeliness', TimelinessListView.as_view(), name='timeliness'),
    path('effectiveness', EffectivenessListView.as_view(),
         name='effectiveness'),
    path('procurement/<int:pk>', ProcurementListView.as_view(),
         name='procurement'),
    path('repairment', RepairmentListView.as_view(), name='repairment'),
]

admin.site.site_header = 'Optimus - Admin'
