from django.views.generic import ListView, DetailView

from supervision.models import (
    OperationalCostSpareParts,
    OperationalCostWorkload)
from web_app.models import (
    Mission, Vehicle, SubSystem)


class TimelinessListView(ListView):
    template_name = "timeliness.html"
    model = Mission

    def get_queryset(self):
        return Mission.objects.none()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        missions = Mission.objects.all()
        context['missions_list'] = missions
        sum = 0
        for item in missions:
            sum = sum + item.delay_abs
        context['delay_sum'] = sum
        return context


class EffectivenessListView(ListView):
    template_name = "effectiveness.html"

    def get_queryset(self):
        return OperationalCostSpareParts.objects.none()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['spare_parts'] = OperationalCostSpareParts.objects.all()
        context['workload'] = OperationalCostWorkload.objects.all()
        return context


class ProcurementListView(DetailView):
    template_name = "procurement.html"
    model = Vehicle

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['subsystems'] = SubSystem.objects.filter(
            vehicle_type=context['vehicle'].type)
        return context


class RepairmentListView(ListView):
    template_name = "repairment.html"
    model = Vehicle
