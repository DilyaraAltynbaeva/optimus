from django.urls import path
from django.contrib import admin

from web_app.views.mission import MissionListView, MissionDetailView
from web_app.views.report import ReportView, generate_report
from web_app.views.vehicle import VehicleView
from web_app.views.prognosis import PrognosisView


urlpatterns = [
    path('', MissionListView.as_view(), name='mission_list'),
    path('mission/<int:pk>', MissionDetailView.as_view(), name='mission'),
    path('mission/<int:pk>/report', ReportView.as_view(), name='report'),
    path('vehicle/<int:pk>', VehicleView.as_view(), name='vehicle'),
    path('report/<int:pk>', generate_report, name='mission_report'),
    path('prognosis/', PrognosisView.as_view(), name='mission prognosis'),
]

admin.site.site_header = 'Optimus - Admin'
